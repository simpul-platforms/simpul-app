package id.co.dapentel.simpul.fragments.datul;


import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;

import id.co.dapentel.simpul.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class AlamatKontakFragment extends Fragment {

    static AlamatKontakFragment instance = null;
    public AlamatKontakFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_datul_alamat_kontak, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        spinnerInit(view);


        Button bt_add_kontak = view.findViewById(R.id.bt_datul_add_kontak);
        ArrayList<LinearLayout> kontak_list = new ArrayList<>();
        LinearLayout kontak_container= view.findViewById(R.id.kontak_list);
        bt_add_kontak.setOnClickListener(v->{
            LinearLayout kontak_item = new LinearLayout(view.getContext());
            kontak_item.setLayoutParams(new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT));
            EditText et_kontak = new EditText(view.getContext());
            et_kontak.setBackgroundResource(R.drawable.edittext_border);
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                    0, LinearLayout.LayoutParams.WRAP_CONTENT, 1);
                    lp.setMargins(4,4,4,4);
            et_kontak.setLayoutParams(lp);
            et_kontak.setHint(R.string.datul_kontak);
            ImageButton bt_kontak_del = new ImageButton(view.getContext());
            bt_kontak_del.setImageDrawable(getResources().getDrawable(R.drawable.ic_close));
            bt_kontak_del.setBackgroundColor(Color.LTGRAY);
            LinearLayout.LayoutParams lp2 = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.MATCH_PARENT);
            lp2.setMargins(4,4,4,4);
            bt_kontak_del.setLayoutParams(lp2);
            bt_kontak_del.setOnClickListener(vi->{
                kontak_container.removeView(kontak_item);
            });
            kontak_item.addView(et_kontak);
            kontak_item.addView(bt_kontak_del);
            kontak_container.addView(kontak_item);
        });
    }

    private void spinnerInit(View view){
        Context context = view.getContext();

        Spinner prov_spinner = view.findViewById(R.id.sp_datul_prov);
        List<String> spinnerArrayProv = new ArrayList<>();
        spinnerArrayProv.add("PROV 1");
        spinnerArrayProv.add("PROV 2");
        spinnerArrayProv.add("PROV 3");

        ArrayAdapter<String> adapter_prov = new ArrayAdapter<>(
                context, android.R.layout.simple_spinner_item, spinnerArrayProv);
        adapter_prov.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        prov_spinner.setAdapter(adapter_prov);

        Spinner kota_spinner = view.findViewById(R.id.sp_datul_kota);
        List<String> spinnerArrayKota = new ArrayList<>();
        spinnerArrayKota.add("Kota 1");
        spinnerArrayKota.add("Kota 2");
        spinnerArrayKota.add("Kota 3");
        spinnerArrayKota.add("Kota 4");

        ArrayAdapter<String> adapter_kota = new ArrayAdapter<>(
                context, android.R.layout.simple_spinner_item, spinnerArrayKota);
        adapter_kota.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        kota_spinner.setAdapter(adapter_kota);

        Spinner kecamaatan_spinner = view.findViewById(R.id.sp_datul_kecamatan);
        List<String> spinnerArrayKecamatan = new ArrayList<>();
        spinnerArrayKecamatan.add("Kecamatan 1");
        spinnerArrayKecamatan.add("Kecamatan 2");
        spinnerArrayKecamatan.add("Kecamatan 3");


        ArrayAdapter<String> adapter_kecamaatan = new ArrayAdapter<>(
                context, android.R.layout.simple_spinner_item, spinnerArrayKecamatan);
        adapter_kecamaatan.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        kecamaatan_spinner.setAdapter(adapter_kecamaatan);

        Spinner kelurahan_spinner = view.findViewById(R.id.sp_datul_kelurahan);
        List<String> spinnerArrayKelurahan = new ArrayList<>();
        spinnerArrayKelurahan.add("Kelurahan 1");
        spinnerArrayKelurahan.add("Kelurahan 2");
        spinnerArrayKelurahan.add("Kelurahan 3");


        ArrayAdapter<String> adapter_kelurahan = new ArrayAdapter<>(
                context, android.R.layout.simple_spinner_item, spinnerArrayKelurahan);
        adapter_kelurahan.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        kelurahan_spinner.setAdapter(adapter_kelurahan);
    }

    /**
     * Returns new instance.
     */
    public static AlamatKontakFragment newInstance(){

        if(instance == null){
            // new instance
            instance = new AlamatKontakFragment();
            return instance;
        } else {
            return instance;
        }
    }
}
