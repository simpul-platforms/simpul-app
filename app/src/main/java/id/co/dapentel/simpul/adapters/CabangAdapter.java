package id.co.dapentel.simpul.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import id.co.dapentel.simpul.R;
import id.co.dapentel.simpul.models.cabang.Pejabat;

public class CabangAdapter extends RecyclerView.Adapter<CabangAdapter.ViewHolder> {
    private List<Pejabat> mData;
    private LayoutInflater mInflater;
    private Context context;

    public CabangAdapter(Context ctx, List<Pejabat> data) {
        this.mInflater = LayoutInflater.from(ctx);
        this.mData = data;
        this.context = ctx;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = mInflater.inflate(R.layout.rc_pejabat_cabang, parent, false);
        return new CabangAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int pos) {
        Pejabat p = mData.get(pos);
        holder.jabatan.setText(p.getJabatan());
        holder.nama.setText(p.getNama());
        for(String kntk : p.getKontak()){
            TextView txt = new TextView(context);
            txt.setText(kntk);
            txt.setLayoutParams(new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT));
            holder.layout_pejabat.addView(txt);
        }

        holder.itemView.setOnClickListener(v -> {
            Toast.makeText(context, "Pejabat "+p.getNama(), Toast.LENGTH_SHORT).show();
        });
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        TextView jabatan, nama;
        LinearLayout layout_pejabat;
        public ViewHolder(@NonNull View v) {
            super(v);
            jabatan = v.findViewById(R.id.jabatan);
            nama = v.findViewById(R.id.tv_rc_nama_pejabat);
            layout_pejabat = v.findViewById(R.id.rc_layout_pejabat);
        }
    }
}