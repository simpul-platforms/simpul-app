package id.co.dapentel.simpul.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import id.co.dapentel.simpul.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class RekanFragment extends Fragment {


    public RekanFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_rekan, container, false);
    }

}
