package id.co.dapentel.simpul.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import id.co.dapentel.simpul.R;
import id.co.dapentel.simpul.models.berita.Umum;

public class BeritaUmumAdapter extends RecyclerView.Adapter<BeritaUmumAdapter.ViewHolder> {
    private ArrayList<Umum> mData;
    private LayoutInflater mInflater;
    private Context context;

    public BeritaUmumAdapter(Context ctx, ArrayList<Umum> data) {
        this.mInflater = LayoutInflater.from(ctx);
        this.mData = data;
        this.context = ctx;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = mInflater.inflate(R.layout.rc_berita_umum, parent, false);
        return new BeritaUmumAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int pos) {
        Umum u = mData.get(pos);
        holder.judul.setText(u.getJudul());
        holder.waktu.setText(u.getWaktu());

        holder.itemView.setOnClickListener(v -> {
            Toast.makeText(context, "Berita ke "+pos, Toast.LENGTH_SHORT).show();
        });
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        TextView judul, waktu;
        public ViewHolder(@NonNull View v) {
            super(v);
            judul = v.findViewById(R.id.tv_rc_umum_judul);
            waktu = v.findViewById(R.id.tv_rc_umum_waktu);
        }
    }
}
