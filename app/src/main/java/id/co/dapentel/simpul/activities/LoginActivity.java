package id.co.dapentel.simpul.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;

import id.co.dapentel.simpul.R;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Button bt_regis = findViewById(R.id.bt_regis);
        bt_regis.setOnClickListener(v->{
            Intent i = new Intent(getBaseContext(), RegisterActivity.class);
            startActivity(i);
        });

        Button bt_login = findViewById(R.id.bt_regis_create);
        bt_login.setOnClickListener(v->{
            Intent i = new Intent(getBaseContext(), MainActivity.class);
            startActivity(i);
            finish();
        });
    }
}
