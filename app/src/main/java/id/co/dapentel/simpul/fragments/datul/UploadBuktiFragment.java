package id.co.dapentel.simpul.fragments.datul;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import id.co.dapentel.simpul.R;

import static android.view.View.inflate;

/**
 * A simple {@link Fragment} subclass.
 */
public class UploadBuktiFragment extends Fragment {

    static UploadBuktiFragment instance = null;
    static List<Integer> id = new ArrayList<>();
    public UploadBuktiFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_datul_upload_bukti, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        LinearLayout layout_file = view.findViewById(R.id.layout_file_bukti);

        addNewFile(layout_file, "KTP");
        addNewFile(layout_file, "Surat Meninggal");
    }

    private void addNewFile(LinearLayout view, String title){
        LinearLayout lin = (LinearLayout) inflate(view.getContext(), R.layout.file_bukti, null);
        TextView t = (TextView) lin.getChildAt(0);
        t.setText(getString(R.string.datul_file, title));
        int new_id = ViewCompat.generateViewId();
        lin.setId(new_id);
        id.add(new_id);
        view.addView(lin);
    }

    public static UploadBuktiFragment newInstance(){

        if(instance == null){
            // new instance
            instance = new UploadBuktiFragment();
            // sets data to bundle
//            Bundle bundle = new Bundle();
//            bundle.putString("msg", text);

            // set data to fragment
//            instance.setArguments(bundle);
            return instance;
        } else {
            return instance;
        }
    }
}
