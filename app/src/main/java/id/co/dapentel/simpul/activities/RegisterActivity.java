package id.co.dapentel.simpul.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;

import id.co.dapentel.simpul.R;

public class RegisterActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        Button bt_regis_create = findViewById(R.id.bt_regis_create);
        bt_regis_create.setOnClickListener(v->{
            finish();
        });
    }
}
