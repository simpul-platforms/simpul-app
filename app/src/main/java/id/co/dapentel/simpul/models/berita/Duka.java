package id.co.dapentel.simpul.models.berita;

public class Duka extends Umum {
    private String waktu_meninggal, waktu_terlapor, pelapor;

    public Duka() {
    }

    public Duka(String judul, String waktu, String isi,
                String waktu_meninggal, String waktu_terlapor,
                String pelapor) {
        super(judul, waktu, isi);
        this.waktu_meninggal = waktu_meninggal;
        this.waktu_terlapor = waktu_terlapor;
        this.pelapor = pelapor;
    }

    public String getWaktu_meninggal() {
        return waktu_meninggal;
    }

    public void setWaktu_meninggal(String waktu_meninggal) {
        this.waktu_meninggal = waktu_meninggal;
    }

    public String getWaktu_terlapor() {
        return waktu_terlapor;
    }

    public void setWaktu_terlapor(String waktu_terlapor) {
        this.waktu_terlapor = waktu_terlapor;
    }

    public String getPelapor() {
        return pelapor;
    }

    public void setPelapor(String pelapor) {
        this.pelapor = pelapor;
    }
}
