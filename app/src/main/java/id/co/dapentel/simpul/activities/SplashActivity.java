package id.co.dapentel.simpul.activities;

import android.content.Intent;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ProgressBar;

import id.co.dapentel.simpul.R;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ProgressBar progressBar = findViewById(R.id.progressBar);
        // timer for seekbar
        final int oneSec = 1100; // 1 minute in milli seconds
        /* CountDownTimer starts with 1 minutes and every onTick is 1 second */
        new CountDownTimer(oneSec, 1000) {
            public void onTick(long millisUntilFinished) {
                //forward progress
                long finishedSeconds = oneSec - millisUntilFinished;
                int total = (int) (((float)finishedSeconds / (float)oneSec) * 100.0+10);
                progressBar.setProgress(total);
            }

            public void onFinish() {
                Intent i = new Intent(getBaseContext(), WelcomeActivity.class);
                startActivity(i);
                finish();
            }
        }.start();
    }
}
