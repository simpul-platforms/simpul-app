package id.co.dapentel.simpul.activities;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import id.co.dapentel.simpul.R;
import id.co.dapentel.simpul.fragments.BeritaFragment;
import id.co.dapentel.simpul.fragments.CabangFragment;
import id.co.dapentel.simpul.fragments.DatulFragment;
import id.co.dapentel.simpul.fragments.LayananFragment;
import id.co.dapentel.simpul.fragments.RekanFragment;

public class MainActivity extends AppCompatActivity {

    private long mBackPressed;
    private static final int TIME_INTERVAL = 2000;
    private BottomNavigationView navigation;

    final Fragment fragment1 = new DatulFragment();
    final Fragment fragment2 = new BeritaFragment();
    final Fragment fragment3 = new RekanFragment();
    final Fragment fragment4 = new LayananFragment();
    final Fragment fragment5 = new CabangFragment();

    final FragmentManager fm = getSupportFragmentManager();
    private Fragment active = fragment1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(null);
        setSupportActionBar(toolbar);

        TextView tv_nama = findViewById(R.id.tv_main_nama);
        tv_nama.setOnClickListener(v->{
            Intent i = new Intent(getBaseContext(), ProfileActivity.class);
            startActivity(i);
        });

        fm.beginTransaction().add(R.id.frame_container, fragment1, "1").commit();
        fm.beginTransaction().add(R.id.frame_container, fragment2, "2").hide(fragment2).commit();
        fm.beginTransaction().add(R.id.frame_container, fragment3, "3").hide(fragment3).commit();
        fm.beginTransaction().add(R.id.frame_container, fragment4, "4").hide(fragment4).commit();
        fm.beginTransaction().add(R.id.frame_container, fragment5, "5").hide(fragment5).commit();

        navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = item -> {
        switch (item.getItemId()) {
            case R.id.navigation_datul:
                swapFragment(fragment1);
                return true;
            case R.id.navigation_berita:
                swapFragment(fragment2);
                return true;
            case R.id.navigation_rekan:
                swapFragment(fragment3);
                return true;
            case R.id.navigation_layanan:
                swapFragment(fragment4);
                return true;
            case R.id.navigation_cabang:
                swapFragment(fragment5);
                return true;
        }
        return false;
    };

    private void swapFragment(Fragment frg){
        fm.beginTransaction().hide(active).show(frg).commit();
        active = frg;
    }

    @Override
    public void onBackPressed() {
        navigation.setSelectedItemId(0);
        if (mBackPressed + TIME_INTERVAL > System.currentTimeMillis())
        {
            finish();
        }
        else { Toast.makeText(getBaseContext(), "Tekan Lagi Untuk Keluar", Toast.LENGTH_SHORT).show(); }

        mBackPressed = System.currentTimeMillis();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }
}
