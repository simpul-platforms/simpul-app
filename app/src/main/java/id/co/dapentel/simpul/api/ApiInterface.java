package id.co.dapentel.simpul.api;

import id.co.dapentel.simpul.models.Token;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ApiInterface {
    @FormUrlEncoded
    @POST("auth/register/")
    Call registerAccount(@Field("nik") String nik,
                         @Field("password") String password,
                         @Field("password2") String password2,
                         @Field("tanggal_lahir") String tanggal_lahir,
                         @Field("email") String email,
                         @Field("kontak") String kontak) ;

    @FormUrlEncoded
    @POST("auth/login/")
    Call<Token> loginAccount(@Field("nik") String nik,
                             @Field("password") String password,
                             @Field("tanggal_lahir") String tanggal_lahir);
}
