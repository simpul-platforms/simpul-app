package id.co.dapentel.simpul.fragments.datul;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;

import id.co.dapentel.simpul.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class CalonPMPFragment extends Fragment {

    static CalonPMPFragment instance = null;

    public CalonPMPFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_datul_calon_pm, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        spinnerInit(view);
    }

    private void spinnerInit(View view){
        Context context = view.getContext();

        Spinner hidup_spinner = view.findViewById(R.id.sp_datul_status_hidup);
        List<String> spinnerArrayHidup = new ArrayList<>();
        spinnerArrayHidup.add("Ya");
        spinnerArrayHidup.add("Tidak");

        ArrayAdapter<String> adapter_hidup = new ArrayAdapter<>(
                context, android.R.layout.simple_spinner_item, spinnerArrayHidup);
        adapter_hidup.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        hidup_spinner.setAdapter(adapter_hidup);

        Spinner jk_spinner = view.findViewById(R.id.sp_datul_jk);
        List<String> spinnerArray = new ArrayList<>();
        spinnerArray.add("Laki-Laki");
        spinnerArray.add("Perempuan");

        ArrayAdapter<String> adapter_jk = new ArrayAdapter<>(
                context, android.R.layout.simple_spinner_item, spinnerArray);
        adapter_jk.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        jk_spinner.setAdapter(adapter_jk);

        Spinner agm_spinner = view.findViewById(R.id.sp_datul_agama);
        List<String> spinnerArrayAgama = new ArrayList<>();
        spinnerArrayAgama.add("Islam");
        spinnerArrayAgama.add("Kristen");
        spinnerArrayAgama.add("Katolik");
        spinnerArrayAgama.add("Hindu");
        spinnerArrayAgama.add("Budha");
        spinnerArrayAgama.add("Konghucu");
        spinnerArrayAgama.add("Penghayat Kepercayaan");

        ArrayAdapter<String> adapter_agama = new ArrayAdapter<>(
                context, android.R.layout.simple_spinner_item, spinnerArrayAgama);
        adapter_agama.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        agm_spinner.setAdapter(adapter_agama);

        Spinner goldar_spinner = view.findViewById(R.id.sp_datul_goldar);
        List<String> spinnerArrayGoldar = new ArrayList<>();
        spinnerArrayGoldar.add("A");
        spinnerArrayGoldar.add("B");
        spinnerArrayGoldar.add("AB");
        spinnerArrayGoldar.add("O");
        spinnerArrayGoldar.add("A+");
        spinnerArrayGoldar.add("B+");
        spinnerArrayGoldar.add("AB+");
        spinnerArrayGoldar.add("O+");
        spinnerArrayGoldar.add("A-");
        spinnerArrayGoldar.add("B-");
        spinnerArrayGoldar.add("AB-");
        spinnerArrayGoldar.add("O-");

        ArrayAdapter<String> adapter_goldar = new ArrayAdapter<>(
                context, android.R.layout.simple_spinner_item, spinnerArrayGoldar);
        adapter_goldar.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        goldar_spinner.setAdapter(adapter_goldar);
    }


    public static CalonPMPFragment newInstance(){

        if(instance == null){
            // new instance
            instance = new CalonPMPFragment();
            // sets data to bundle
//            Bundle bundle = new Bundle();
//            bundle.putString("msg", text);

            // set data to fragment
//            instance.setArguments(bundle);
            return instance;
        } else {
            return instance;
        }
    }
}
