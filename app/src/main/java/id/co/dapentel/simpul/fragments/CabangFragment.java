package id.co.dapentel.simpul.fragments;


import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;

import id.co.dapentel.simpul.R;
import id.co.dapentel.simpul.activities.MainActivity;
import id.co.dapentel.simpul.adapters.CabangAdapter;
import id.co.dapentel.simpul.models.cabang.Pejabat;

/**
 * A simple {@link Fragment} subclass.
 */
public class CabangFragment extends Fragment {

    private Activity activity;
    RecyclerView rc_pejabat_cabang;
    CabangAdapter cabang_adapter;
    List<Pejabat> data_pejabat = new ArrayList<>();

    public CabangFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_cabang, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // TODO SPINNER
        Spinner cb_spinner = view.findViewById(R.id.spinner_cabang);

        List<String> spinnerArray = new ArrayList<>();
        spinnerArray.add("Bandung Timur");
        spinnerArray.add("Bandung Barat");

        ArrayAdapter<String> adapter = new ArrayAdapter<>(
                activity, android.R.layout.simple_spinner_item, spinnerArray);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        cb_spinner.setAdapter(adapter);

        //TODO RC VIEW
        dummy_data();
        rc_pejabat_cabang = view.findViewById(R.id.rc_cabang);
        cabang_adapter = new CabangAdapter(activity, data_pejabat);

        LinearLayoutManager layoutManager = new LinearLayoutManager(activity);
//        layoutManager.setReverseLayout(true);
//        layoutManager.setStackFromEnd(true);
        rc_pejabat_cabang.setLayoutManager(layoutManager);
        rc_pejabat_cabang.setAdapter(cabang_adapter);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rc_pejabat_cabang.getContext(),
                layoutManager.getOrientation());
        rc_pejabat_cabang.addItemDecoration(dividerItemDecoration);
    }

    public void onAttach(Context context) {
        super.onAttach(context);
        if (activity == null && context instanceof MainActivity) {
            activity = (MainActivity) context;
        }
    }

    @Override
    public void onDetach() {
        this.activity = null;
        super.onDetach();
    }

    private void dummy_data(){
        List<String> dummy_kontak = new ArrayList<>();
        dummy_kontak.add("pensiunan@dapentel.co.id");
        dummy_kontak.add("12323232");
        dummy_kontak.add("1213243425252");
        dummy_kontak.add("198218281282");

        data_pejabat.add(new Pejabat("Ketua","Awaludin", dummy_kontak));
        data_pejabat.add(new Pejabat("Wakil Ketua","Samarudin", dummy_kontak));
        data_pejabat.add(new Pejabat("Sekretaris","Somarudin", dummy_kontak));
        data_pejabat.add(new Pejabat("Bendahara","Aminudin", dummy_kontak));
        data_pejabat.add(new Pejabat("Pengawas","Akhirudin", dummy_kontak));
    }
}
