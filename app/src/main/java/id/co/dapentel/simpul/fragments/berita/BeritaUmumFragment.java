package id.co.dapentel.simpul.fragments.berita;


import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import id.co.dapentel.simpul.R;
import id.co.dapentel.simpul.activities.MainActivity;
import id.co.dapentel.simpul.adapters.BeritaUmumAdapter;
import id.co.dapentel.simpul.models.berita.Umum;

/**
 * A simple {@link Fragment} subclass.
 */
public class BeritaUmumFragment extends Fragment {

    private Activity activity;
    private RecyclerView rc_berita_umum;
    private BeritaUmumAdapter umum_adapter;
    private ArrayList<Umum> data_umum = new ArrayList<>();

    public BeritaUmumFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_berita_umum, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        for (int i = 0; i<10; i+=1){
            dummy_data();
        }
        rc_berita_umum = view.findViewById(R.id.rc_berita_umum);
        umum_adapter = new BeritaUmumAdapter(activity, data_umum);

        LinearLayoutManager layoutManager = new LinearLayoutManager(activity);
        layoutManager.setReverseLayout(true);
        layoutManager.setStackFromEnd(true);
        rc_berita_umum.setLayoutManager(layoutManager);
        rc_berita_umum.setAdapter(umum_adapter);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rc_berita_umum.getContext(),
                layoutManager.getOrientation());
        rc_berita_umum.addItemDecoration(dividerItemDecoration);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (activity == null && context instanceof MainActivity) {
            activity = (MainActivity) context;
        }
    }

    @Override
    public void onDetach() {
        this.activity = null;
        super.onDetach();
    }

    private void dummy_data(){
        data_umum.add(new Umum("Serah Terima Hasil Review Atas Laporan Portofolio Investasi dan Laporan Keuangan Dapentel 30 Juni 2018",
                "17 Juli 2018", ""));
        data_umum.add(new Umum("Kick of development manajemen ISO 9001",
                "18 Juli 2018", ""));
        data_umum.add(new Umum("Silaturahim Bersama karayawan dapentel",
                "19 Juli 2018", ""));
    }
}
