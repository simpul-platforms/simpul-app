package id.co.dapentel.simpul.fragments.berita;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import id.co.dapentel.simpul.R;
import id.co.dapentel.simpul.activities.MainActivity;
import id.co.dapentel.simpul.adapters.BeritaDukaAdapter;
import id.co.dapentel.simpul.models.berita.Duka;

/**
 * A simple {@link Fragment} subclass.
 */
public class BeritaDukaFragment extends Fragment {

    private Activity activity;
    private RecyclerView rc_berita_duka;
    private BeritaDukaAdapter umum_adapter;
    private ArrayList<Duka> data_duka = new ArrayList<>();


    public BeritaDukaFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_berita_duka, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        for (int i = 0; i<10; i+=1){
            dummy_data();
        }
        rc_berita_duka = view.findViewById(R.id.rc_berita_duka);
        umum_adapter = new BeritaDukaAdapter(activity, data_duka);

        LinearLayoutManager layoutManager = new LinearLayoutManager(activity);
        layoutManager.setReverseLayout(true);
        layoutManager.setStackFromEnd(true);
        rc_berita_duka.setLayoutManager(layoutManager);
        rc_berita_duka.setAdapter(umum_adapter);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rc_berita_duka.getContext(),
                layoutManager.getOrientation());
        rc_berita_duka.addItemDecoration(dividerItemDecoration);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (activity == null && context instanceof MainActivity) {
            activity = (MainActivity) context;
        }
    }

    @Override
    public void onDetach() {
        this.activity = null;
        super.onDetach();
    }

    private void dummy_data(){
        data_duka.add(new Duka("Solehudin",
                "17 Juli 2018", "", "CB ujung berung 19 juli 2018", "dilaporkan CB ujung berung 20 juli 2018", ""));
        data_duka.add(new Duka("Richard",
                "18 Juli 2018", "", "CB ujung berung 19 juli 2018", "dilaporkan CB ujung berung 20 juli 2018", ""));
        data_duka.add(new Duka("Reyanld",
                "19 Juli 2018", "", "CB ujung berung 19 juli 2018", "dilaporkan CB ujung berung 20 juli 2018", ""));
    }
}
