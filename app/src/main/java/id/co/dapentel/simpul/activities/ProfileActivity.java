package id.co.dapentel.simpul.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.widget.ExpandableListView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import id.co.dapentel.simpul.R;
import id.co.dapentel.simpul.adapters.ProfileExpendableAdapter;
import id.co.dapentel.simpul.models.profile.expandable.ProfileListItem;

public class ProfileActivity extends AppCompatActivity {

    private ProfileExpendableAdapter listAdapter;
    private ExpandableListView expListView;
    private List<String> listDataHeader;
    private HashMap<String, List<ProfileListItem>> listDataChild;
    private DisplayMetrics metrics;
    private int width;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Profile");
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        // get the listview
        expListView = findViewById(R.id.exp_list_profile);

        // preparing list data
        prepareListData();

        listAdapter = new ProfileExpendableAdapter(this, listDataHeader, listDataChild);

        //initialze displayMetrics
        metrics = new DisplayMetrics();
        //get the metrics of window
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        //save width of window
        width = metrics.widthPixels;
        //check version of sdk(android version)
        if(android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN_MR2) {
            //For sdk version bellow 18
            expListView.setIndicatorBounds(width - GetDipsFromPixel(50), width - GetDipsFromPixel(10));
        } else {
            //For sdk 18 and above
            expListView.setIndicatorBoundsRelative(width - GetDipsFromPixel(50), width - GetDipsFromPixel(10));
        }

        // setting list adapter
        expListView.setAdapter(listAdapter);
    }

    private void prepareListData() {
        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<ProfileListItem>>();

        listDataHeader.add("Biodata");
        listDataHeader.add("Data Diri");
        listDataHeader.add("Alamat");
        listDataHeader.add("Kontak");

        List<ProfileListItem> biodata = new ArrayList<>();
        biodata.add(new ProfileListItem("NIK", "531968"));
        biodata.add(new ProfileListItem("NAW", "1"));
        biodata.add(new ProfileListItem("Nama Pensiunan", getString(R.string.dummy_name)));
        biodata.add(new ProfileListItem("Tanggal Lahir", "12 Juni 1950"));

        List<ProfileListItem> data_diri = new ArrayList<>();
        data_diri.add(new ProfileListItem("KTP", "1212143432"));
        data_diri.add(new ProfileListItem("NPWP", "43432"));
        data_diri.add(new ProfileListItem("Agama", "Islam"));
        data_diri.add(new ProfileListItem("Golongan Darah", "A-"));
        data_diri.add(new ProfileListItem("Jenis Kelamin", "Laki-Laki"));

        List<ProfileListItem> alamat = new ArrayList<>();
        alamat.add(new ProfileListItem("Rumah", "Komp mekar Baroka b100"));
        alamat.add(new ProfileListItem("RT/RW", "2/12"));
        alamat.add(new ProfileListItem("Kelurahan", "Cibiru Tengah"));
        alamat.add(new ProfileListItem("Kecamatan", "Cileunyi"));
        alamat.add(new ProfileListItem("Kabupatan/Kota", "Kabupaten Bandung"));
        alamat.add(new ProfileListItem("Provinsi", "Jawa Barat"));
        alamat.add(new ProfileListItem("Kode Pos", "140132"));


        List<ProfileListItem> kontak = new ArrayList<>();
        kontak.add(new ProfileListItem("Rumah","1234556"));
        kontak.add(new ProfileListItem("HP","1234232556"));
        kontak.add(new ProfileListItem("HP","1234232556"));

        listDataChild.put(listDataHeader.get(0), biodata);
        listDataChild.put(listDataHeader.get(1), data_diri);
        listDataChild.put(listDataHeader.get(2), alamat);
        listDataChild.put(listDataHeader.get(3), kontak);
    }

    private int GetDipsFromPixel(int pixels) {
        // Get the screen's density scale
        final float scale = getResources().getDisplayMetrics().density;
        // Convert the dps to pixels, based on density scale
        return (int) (pixels * scale + 0.5f);
    }
}
