package id.co.dapentel.simpul.fragments.datul;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import id.co.dapentel.simpul.R;
import id.co.dapentel.simpul.custom_view.HTMLTextView;

/**
 * A simple {@link Fragment} subclass.
 */
public class KonfirmasiFragment extends Fragment {

    static KonfirmasiFragment instance = null;
    public KonfirmasiFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_datul_konfirmasi, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        HTMLTextView persetujuan = view.findViewById(R.id.tv_persetujuan);
        String isi_persetujuan = "Dengan Ini Saya Menyatakan Bahwa : " +
                "<ol style='text-align: justify; text-justify: inter-word;'>" +
                "<li>Saya mengisikan data sebenar-benarnya dan tidak ditutup-tutupi.</li>" +
                "<li>Saya bertanggung jawab penuh terhadap data yang saya isi.</li>" +
                "</ol>";
        persetujuan.init(isi_persetujuan);
        persetujuan.setTextSize(16);

        Button bt_finish = view.findViewById(R.id.bt_datul_finish);
        bt_finish.setOnClickListener(v->{
            Activity act = (Activity) view.getContext();
            act.finish();
        });

    }

    public static KonfirmasiFragment newInstance(){

        if(instance == null){
            // new instance
            instance = new KonfirmasiFragment();
            // sets data to bundle
//            Bundle bundle = new Bundle();
//            bundle.putString("msg", text);

            // set data to fragment
//            instance.setArguments(bundle);
            return instance;
        } else {
            return instance;
        }
    }

}
