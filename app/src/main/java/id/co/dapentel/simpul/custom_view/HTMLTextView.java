package id.co.dapentel.simpul.custom_view;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;

public class HTMLTextView extends WebView {
    public HTMLTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
        getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        setBackgroundColor(Color.TRANSPARENT);
    }

    public void init(String text){
        loadData(text, "text/html; charset=utf-8", "UTF-8");
    }

    public void arrayInit(@NonNull String[] x){
        StringBuilder res = new StringBuilder("<ol type='1' " +
                "style='text-align: justify; text-justify: inter-word;" +
                "padding-left: 0; margin-left: 1em;'>");
        for(String st : x){
            res.append("<li>");
            res.append(st);
            res.append("</li>");
        }
        res.append("</ol>");
        loadData(res.toString(), "text/html; charset=utf-8", "UTF-8");
    }

    public void setTextSize(int size){
        getSettings().setDefaultFontSize(size);
    }
}
