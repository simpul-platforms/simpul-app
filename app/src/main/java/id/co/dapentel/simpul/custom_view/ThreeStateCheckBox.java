package id.co.dapentel.simpul.custom_view;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.AppCompatCheckBox;
import android.util.AttributeSet;

import id.co.dapentel.simpul.R;

public class ThreeStateCheckBox extends AppCompatCheckBox
    {
        static private final int UNKNOW = -1;
        static private final int UNCHECKED = 0;
        static private final int CHECKED = 1;
        private int state;

        public ThreeStateCheckBox(Context context) {
            super(context);
            init();
        }

        public ThreeStateCheckBox(Context context, AttributeSet attrs) {
            super(context, attrs);
            init();
        }

        public ThreeStateCheckBox(Context context, AttributeSet attrs, int defStyleAttr) {
            super(context, attrs, defStyleAttr);
            init();
        }

        private void init()
        {
            state = UNKNOW;
            updateBtn();

            // checkbox status is changed from uncheck to checked.
            setOnCheckedChangeListener((buttonView, isChecked) -> {
                switch (state)
                {
                    case UNKNOW:
                        state = UNCHECKED;
                        break;
                    case UNCHECKED:
                        state = CHECKED;
                        break;
                    case CHECKED:
                        // No need to go back to Initial State
                        // state = UNCHECKED
                        state = UNCHECKED;
                        break;
                }
                updateBtn();
            });

        }

        private void updateBtn()
        {
            switch (state)
            {
                case UNKNOW:
                    setButtonDrawable(R.drawable.ic_do_not_disturb);
                    setBackgroundColor(Color.LTGRAY);
                    break;
                case UNCHECKED:
                    setButtonDrawable(R.drawable.ic_close);
                    setBackgroundColor(Color.RED);
                    break;
                case CHECKED:
                    setButtonDrawable(R.drawable.ic_check);
                    setBackgroundColor(Color.GREEN);
                    break;
            }


        }
        public int getState()
        {
            return state;
        }

        public void setState(int state)
        {
            this.state = state;
            updateBtn();
        }
    }

