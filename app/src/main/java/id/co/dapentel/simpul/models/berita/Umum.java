package id.co.dapentel.simpul.models.berita;

public class Umum {
    private String judul, waktu, isi;

    public Umum() {
    }

    public Umum(String judul, String waktu, String isi) {
        this.judul = judul;
        this.waktu = waktu;
        this.isi = isi;
    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getWaktu() {
        return waktu;
    }

    public void setWaktu(String waktu) {
        this.waktu = waktu;
    }

    public String getIsi() {
        return isi;
    }

    public void setIsi(String isi) {
        this.isi = isi;
    }
}
