package id.co.dapentel.simpul.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import id.co.dapentel.simpul.R;
import id.co.dapentel.simpul.models.berita.Duka;

public class BeritaDukaAdapter extends RecyclerView.Adapter<BeritaDukaAdapter.ViewHolder> {
    private ArrayList<Duka> mData;
    private LayoutInflater mInflater;
    private Context context;

    public BeritaDukaAdapter(Context ctx, ArrayList<Duka> data) {
        this.mInflater = LayoutInflater.from(ctx);
        this.mData = data;
        this.context = ctx;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = mInflater.inflate(R.layout.rc_berita_duka, parent, false);
        return new BeritaDukaAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int pos) {
        Duka d = mData.get(pos);
        holder.pmp.setText(d.getJudul());
        holder.waktu_meninggal.setText(d.getWaktu_meninggal());
        holder.waktu_lapor.setText(d.getWaktu_terlapor());


        holder.itemView.setOnClickListener(v -> {
            Toast.makeText(context, "Berita duka ke "+pos, Toast.LENGTH_SHORT).show();
        });
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        TextView pmp, waktu_lapor, waktu_meninggal;
        public ViewHolder(@NonNull View v) {
            super(v);
            pmp = v.findViewById(R.id.tv_rc_duka_pmp);
            waktu_meninggal = v.findViewById(R.id.tv_rc_duka_waktu_meninggal);
            waktu_lapor = v.findViewById(R.id.tv_rc_duka_waktu_lapor);
        }
    }
}
