package id.co.dapentel.simpul.models.cabang;

import java.util.List;

public class Pejabat {
    private String jabatan, nama;
    private List<String> kontak;

    public Pejabat() {
    }

    public Pejabat(String jabatan, String nama, List<String> kontak) {
        this.jabatan = jabatan;
        this.nama = nama;
        this.kontak = kontak;
    }

    public String getJabatan() {
        return jabatan;
    }

    public void setJabatan(String jabatan) {
        this.jabatan = jabatan;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public List<String> getKontak() {
        return kontak;
    }

    public void setKontak(List<String> kontak) {
        this.kontak = kontak;
    }
}
