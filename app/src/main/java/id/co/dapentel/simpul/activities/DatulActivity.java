package id.co.dapentel.simpul.activities;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.kofigyan.stateprogressbar.StateProgressBar;

import java.util.Objects;

import id.co.dapentel.simpul.R;
import id.co.dapentel.simpul.fragments.datul.AlamatKontakFragment;
import id.co.dapentel.simpul.fragments.datul.CalonPMPFragment;
import id.co.dapentel.simpul.fragments.datul.DataDiriFragment;
import id.co.dapentel.simpul.fragments.datul.KonfirmasiFragment;
import id.co.dapentel.simpul.fragments.datul.UploadBuktiFragment;
import id.co.dapentel.simpul.custom_view.NoSwipeViewPager;

public class DatulActivity extends AppCompatActivity{

    private static final int NUMBER_OF_PAGES = 5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_datul);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Data Ulang");
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        String[] descriptionData = {"Data\nDiri", "Alamat &\nKontak", "Calon\nPMP", "Upload\nBukti", "Selesai"};
        StateProgressBar stateProgressBar = findViewById(R.id.state_prog_datul);
        stateProgressBar.setStateDescriptionData(descriptionData);
        stateProgressBar.setCurrentStateNumber(state_number(0));
        NoSwipeViewPager datul_pager = findViewById(R.id.pager_datul);
        datul_pager.setAdapter(new MyPagerAdapter(getSupportFragmentManager()));
        datul_pager.setPagingEnabled(false);

        Button bt_next = findViewById(R.id.bt_datul_next);
        Button bt_back = findViewById(R.id.bt_datul_back);
        bt_back.setVisibility(View.INVISIBLE);

        bt_next.setOnClickListener(v->{
            if (datul_pager.getCurrentItem() < 4){
                int next = datul_pager.getCurrentItem()+1;
                bt_back.setVisibility(View.VISIBLE);
                stateProgressBar.setCurrentStateNumber(state_number(next));
                datul_pager.setCurrentItem(next, true);
                if(datul_pager.getCurrentItem()==4){
                    bt_next.setVisibility(View.INVISIBLE);
                }
            }
        });

        bt_back.setOnClickListener(v->{
            if (datul_pager.getCurrentItem() > 0){
                int prev = datul_pager.getCurrentItem()-1;
                bt_next.setVisibility(View.VISIBLE);
                stateProgressBar.setCurrentStateNumber(state_number(prev));
                datul_pager.setCurrentItem(prev, true);
                if(datul_pager.getCurrentItem()==0){
                    bt_back.setVisibility(View.INVISIBLE);
                }
            }
        });
    }

    private StateProgressBar.StateNumber state_number(int i){
        if (i==0)
            return StateProgressBar.StateNumber.ONE;
        else if (i==1)
            return StateProgressBar.StateNumber.TWO;
        else if (i==2)
            return StateProgressBar.StateNumber.THREE;
        else if (i==3)
            return StateProgressBar.StateNumber.FOUR;
        else if (i==4)
            return StateProgressBar.StateNumber.FIVE;
        else
            return StateProgressBar.StateNumber.ONE;
    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setMessage(getString(R.string.dialog_datul_exit))
                .setCancelable(false)
                .setPositiveButton("Keluar", (dialog, id) -> DatulActivity.super.onBackPressed())
                .setNegativeButton("Lanjut Datul", null)
                .show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return false;
    }

    private class MyPagerAdapter extends FragmentPagerAdapter {

        /**
         * Constructor
         */
        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        /**
         * Return fragment based on the position.
         */
        @Override
        public Fragment getItem(int position) {
            switch(position) {
                case 0:
                    return DataDiriFragment.newInstance();
                case 1:
                    return AlamatKontakFragment.newInstance();
                case 2:
                    return CalonPMPFragment.newInstance();
                case 3:
                    return UploadBuktiFragment.newInstance();
                case 4:
                    return KonfirmasiFragment.newInstance();
                default:
                    return DataDiriFragment.newInstance();
            }
        }

        /**
         * Return the number of pages.
         */
        @Override
        public int getCount() {
            return NUMBER_OF_PAGES;
        }

    }

}
